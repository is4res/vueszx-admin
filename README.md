# Vueszx Admin
[![build status](https://gitlab.com/is4res/vueszx-admin/badges/develop/build.svg)](https://gitlab.com/is4res/vueszx-admin/pipelines)
[![coverage report](https://gitlab.com/is4res/vueszx-admin/badges/develop/coverage.svg)](https://gitlab.com/is4res/vueszx-admin/commits/develop)

## Introduction
Vueszx Admin is using [Bootstrap Vue](https://bootstrap-vue.js.org/) as base framework.

## Demo
- [Live](https://vue.biszx.com/#/)
- [storybook](https://vue.biszx.com/storybook/)

## Project setup
Before getting started, make sure you have all prerequisites installed
- Node.js (>=8.9)
- npm version 3+ (or yarn version 1.16+) and Git.

Install dependencies
```
yarn install
```
#### Vue

Compiles and hot-reloads for development
```
yarn serve
```

Compiles and minifies for production
```
yarn build:prod
```

Lints and fixes files
```
yarn lint
```

Run your unit tests
```
yarn test:unit
```

Run your end-to-end tests
```
yarn test:e2e
```

#### Storybook

Compiles and hot-reloads for development
```
yarn storybook:serve
```

Compiles and minifies for production
```
yarn storybook:build
```

## License
[MIT](https://gitlab.com/is4res/vueszx-admin/blob/master/LICENSE) License

## Test Tools
#### Unit Test
Using [Jest](https://jestjs.io/) have main file in [tests/unit/specs/main.js](tests/unit/specs/main.js) for settings vue to test and mount component on function setup

#### E2E Test
Using [cypress](https://www.cypress.io/)
