import Component from '@/views/alert/Alert'
import { vm, setup } from '../../main'

describe('Alert', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('success', async () => {
    await vm.success()
    expect(vm.$store.getters.alert.show).toEqual(true)
    expect(vm.$store.getters.alert.status).toEqual('success')
  })

  it('fail', async () => {
    await vm.fail()
    expect(vm.$store.getters.alert.show).toEqual(true)
    expect(vm.$store.getters.alert.status).toEqual('fail')
  })

  it('custom', async () => {
    await vm.custom()
    expect(vm.$store.getters.alert.show).toEqual(true)
    expect(vm.$store.getters.alert.status).toEqual('custom')
    expect(vm.$store.getters.alert.type).toEqual('warning')
    expect(vm.$store.getters.alert.title).toEqual('Custom')
    expect(vm.$store.getters.alert.text).toEqual('more')
  })
})
