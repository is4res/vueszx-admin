import Component from '@/views/data/table/TableServerside'
import { vm, setup, wrapper } from '../../../main'

describe('TableServerside', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('initData', () => {
    vm.getItems = jest.fn()
    vm.initData()
    expect(vm.getItems).toBeCalledTimes(1)
  })

  it('getItems', async () => {
    vm.$api = () => {
      return new Promise((resolve, reject) => {
        resolve({
          headers: {
            'x-total-count': 1
          },
          data: [
            {
              id: 1
            }
          ]
        })
      })
    }
    await vm.getItems({
      filter: '',
      sortBy: '',
      sortDesc: 'asc',
      page: 1,
      perPage: 10,
    })
    expect(vm.totalRows).toEqual(1)
    expect(vm.items).toHaveLength(1)
  })

  it('edit', () => {
    const item = {
      id: 1
    }
    vm.edit(item)
    expect(vm.update_data).toEqual(item)
  })

  it('$submitDelete', async () => {
    wrapper.setData({
      items: [
        {
          id: 1
        }
      ]
    })
    let beforeLength = vm.items.length
    await vm.$submitDelete([1])
    expect(vm.items).toHaveLength(beforeLength - 1)
  })
})
