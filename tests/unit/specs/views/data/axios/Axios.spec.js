import Component from '@/views/data/axios/Axios'
import { vm, setup } from '../../../main'

describe('DataAxios', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('getCurrency', async () => {
    // response unsuccess
    vm.$api = (request) => {
      return new Promise(function (resolve, reject) {
        reject(new Error('test catch'))
      })
    }
    await vm.getCurrency()

    // response success
    vm.$api = (request) => {
      return new Promise(function (resolve, reject) {
        resolve({
          data: {}
        })
      })
    }
    await vm.getCurrency()
    expect(vm.data).toEqual({})
  })
})
