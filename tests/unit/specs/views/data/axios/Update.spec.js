import Component from '@/views/data/axios/Update'
import { wrapper, vm, setup } from '../../../main'

describe('Update-DataAxios', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        submit: false,
        close: false
      }
    })
  })

  it('watch:submit', async () => {
    vm.onSubmit = jest.fn()
    wrapper.setProps({
      submit: true
    })
    await vm.$nextTick(() => {
      expect(vm.onSubmit).toBeCalledTimes(1)
    })

    vm.onSubmit = jest.fn()
    wrapper.setProps({
      submit: false
    })
    await vm.$nextTick(() => {
      expect(vm.onSubmit).toBeCalledTimes(0)
    })
  })

  it('watch:close', async () => {
    vm.onClose = jest.fn()
    wrapper.setProps({
      close: true
    })
    await vm.$nextTick(() => {
      expect(vm.onClose).toBeCalledTimes(1)
    })

    vm.onClose = jest.fn()
    wrapper.setProps({
      close: false
    })
    await vm.$nextTick(() => {
      expect(vm.onClose).toBeCalledTimes(0)
    })
  })

  it('onSubmit', async () => {
    vm.$bvModal.hide = jest.fn()
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(true)
      })
    }
    await vm.onSubmit()
    expect(wrapper.emitted('update:submit')).toBeTruthy()
    expect(vm.$bvModal.hide).toBeCalledTimes(1)

    vm.$bvModal.hide = jest.fn()
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(false)
      })
    }
    await vm.onSubmit()
    expect(wrapper.emitted('update:submit')).toBeTruthy()
    expect(vm.$bvModal.hide).toBeCalledTimes(0)
  })

  it('onClose', async () => {
    wrapper.setData({
      formData: {
        a: 'test'
      }
    })
    await vm.onClose()
    expect(vm.formData).toEqual({})
    expect(wrapper.emitted('update:close')).toBeTruthy()
  })
})
