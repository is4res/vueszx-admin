import Component from '@/views/user/profile/Profile'
import { setup } from '../../../main'

describe('Profile', async () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('init', () => {
    'Noting to test'
  })
})
