import Component from '@/views/user/profile/BasicSettings'
import { vm, setup } from '../../../main'

const bvModalEvt = {
  preventDefault: () => true
}

describe('BasicSettings', async () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('onSubmit', () => {
    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(true)
      })
    }
    vm.onSubmit(bvModalEvt)

    vm.$validator.validateAll = () => {
      return new Promise(resolve => {
        resolve(false)
      })
    }
    vm.onSubmit(bvModalEvt)
  })
})
