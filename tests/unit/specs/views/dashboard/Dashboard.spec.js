import Component from '@/views/dashboard/Dashboard'
import { setup } from '../../main'

describe('Dashboard', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('init', () => {
    'Nothing to test'
  })
})
