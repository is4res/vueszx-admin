import Component from '@/components/layouts/app/app-navbar/dropdowns/LanguageDropdown'
import { vm, setup } from '../../../main'

describe('LanguageDropdowns', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('setLanguage', () => {
    vm.setLanguage('th')
    expect(vm.$i18n.locale()).toEqual('th')
    expect(vm.$store.getters.config.locale).toEqual('th')
  })

  it('currentLanguage', () => {
    vm.$i18n.set('en')
    expect(vm.currentLanguage()).toBe('gb')

    vm.$i18n.set('th')
    expect(vm.currentLanguage()).toBe('th')
  })

  it('currentLanguageName', () => {
    vm.$i18n.set('gb')
    expect(vm.currentLanguageName()).toEqual(vm.$t('language.en'))

    vm.$i18n.set('th')
    expect(vm.currentLanguageName()).toEqual(vm.$t('language.th'))
  })

  it('flagIconClass', () => {
    const code = ''
    expect(vm.flagIconClass(code)).toEqual(`flag-icon-${code}`)
  })
})
