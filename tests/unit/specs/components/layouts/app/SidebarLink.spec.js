import Component from '@/components/layouts/app/app-sidebar/components/SidebarLink'
import { vm, setup } from '../../../main'

describe('SidebarLink', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        to: {
          name: 'dashboard'
        }
      }
    })
  })

  it('watch:$route', async () => {
    vm.$router.push({ name: 'user-profile' })
    await vm.$nextTick()

    vm.$children[0].$el.classList.contains = (value) => {
      return true
    }
    vm.$parent = {
      $parent: {
        $options: {
          name: 'sidebar-link-group'
        },
      }
    }
    vm.$router.push({ name: 'dashboard' })
    await vm.$nextTick()

    vm.$parent = {
      $parent: {
        $options: {
          name: ''
        },
      }
    }
    vm.$router.push({ name: 'user-profile' })
  })
})
