import Component from '@/components/layouts/app/app-navbar/AppNavbar'
import { wrapper, vm, setup } from '../../../main'

describe('AppNavbar', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component)
  })

  it('toggleSidebar', () => {
    let isOpen = true
    wrapper.setData({
      isOpen: isOpen
    })
    vm.toggleSidebar()
    expect(vm.isOpen).toEqual(!isOpen)
    expect(vm.$store.getters.classToggleSidebar).toEqual('sidebar-toggle')

    isOpen = false
    wrapper.setData({
      isOpen: isOpen
    })
    vm.toggleSidebar()
    expect(vm.isOpen).toEqual(!isOpen)
    expect(vm.$store.getters.classToggleSidebar).toEqual('')
  })
})
