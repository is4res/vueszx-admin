import Component from '@/components/layouts/app/app-sidebar/AppSidebar'
import { setup } from '../../../main'

describe('AppSidebar', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        isOpen: true
      }
    })
  })

  it('init', () => {
    'Nothing to test'
  })
})
