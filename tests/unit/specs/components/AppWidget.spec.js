import Component from '@/components/AppWidget'
import { wrapper, vm, setup } from '../main'

describe('AppWidget', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()

    // setting component
    setup(Component, {
      propsData: {
        app: 'test',
        textHeader: 'TestHeader',
        bgHeader: 'primary',
        colorHeader: 'black',
        hasAdd: true,
        hasDelete: true
      }
    })
  })

  it('emitClickAdd', async () => {
    await vm.emitClickAdd()
    expect(wrapper.emitted('click-add')).toBeTruthy()
  })

  it('emitClickDelete', async () => {
    await vm.emitClickDelete()
    expect(wrapper.emitted('click-delete')).toBeTruthy()
  })
})
