import { token, permission } from '@/router/middleware'

describe('Middleware', () => {
  beforeEach(() => {
    // clear mock
    jest.resetModules()
    jest.clearAllMocks()
  })

  it('token', () => {
    // not have token
    expect(token()).toBeFalsy()

    // have token
    localStorage.setItem('jwt', 'test@token')
    expect(token()).toBeTruthy()
  })

  it('permission', () => {
    let required = ['test.view']
    let userPermission = []

    // not have permission
    expect(permission(required, userPermission)).toBeFalsy()

    // have permission
    userPermission = ['test.view']
    expect(permission(required, userPermission)).toBeTruthy()

    // have permission cause not have required
    required = []
    expect(permission(required, userPermission)).toBeTruthy()
  })
})
