import AppException from './AppException'
import AppWidget from './AppWidget'
import AppModal from './AppModal'
import AppTable from './AppTable'

const ComponentPlugin = {
  install (Vue, options) {
    [
      AppException,
      AppWidget,
      AppModal,
      AppTable
    ].forEach(component => {
      Vue.component(component.name, component)
    })
  },
}

export default ComponentPlugin
