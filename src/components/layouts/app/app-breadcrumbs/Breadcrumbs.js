export const breadcrumbs = {
  root: {
    name: 'dashboard',
    displayName: 'menu.home',
  },
  routes: [
    {
      name: 'dashboard',
      displayName: 'menu.dashboard',
    },
    {
      name: 'data',
      displayName: 'menu.data._',
      disabled: true,
      children: [
        {
          name: 'data-axios',
          displayName: 'menu.data.axios',
        },
        {
          name: 'data-table',
          displayName: 'menu.data.table',
        },
        {
          name: 'data-tableServerside',
          displayName: 'menu.data.tableServerside',
        },
      ],
    },
    {
      name: 'form-data',
      displayName: 'menu.formData',
    },
    {
      name: 'alert',
      displayName: 'menu.alert',
    },
    {
      name: 'user',
      displayName: 'menu.user',
      disabled: true,
      children: [
        {
          name: 'user-profile',
          displayName: 'menu.userProfile',
        }
      ],
    },
  ],
}
