/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import AppTable from '@/components/AppTable'
import store from '@/store'
import { object, boolean } from '@storybook/addon-knobs'
import { action } from '@storybook/addon-actions'

export const actionMethods = {
  onClickEdit: action('show update modal'),
  onClickDelete: action('show delete modal')
}

storiesOf('Table', module)
  .add('Using',
    () => ({
      store,
      components: {
        AppTable
      },
      props: {
        items: {
          default: object('Items', [
            {
              id: 1,
              title: 'A Title',
              detail: '2 Detail'
            },
            {
              id: 2,
              title: 'B Title',
              detail: '1 Detail'
            }
          ])
        },
        fields: {
          default: object('Fields', [
            {
              key: 'select',
              label: 'Select'
            },
            {
              key: 'title',
              label: 'Title',
              sortable: true,
            },
            {
              key: 'detail',
              label: 'Detail',
              sortable: true,
            },
            {
              key: 'actions',
              label: 'Actions'
            }
          ])
        },
        hasUpdate: {
          default: boolean('hasUpdate', true)
        },
        hasDelete: {
          default: boolean('hasDelete', true)
        }
      },
      data () {
        return {
          filter: '',
          perPage: 10,
          currentPage: 1,
          totalRows: 0,
          selected: [],
        }
      },
      mounted () {
        // set store data
        this.$store.commit('setUserPerm', [
          'stories.update',
          'stories.delete'
        ])
      },
      methods: {
        ...actionMethods
      },
      template: `
        <div>
          <b-container>
            <app-table
              app="stories"
              :hasUpdate="hasUpdate"
              :hasDelete="hasDelete"
              :filter.sync="filter"
              :fields="fields"
              :items="items"
              :perPage.sync="perPage"
              :currentPage.sync="currentPage"
              :totalRows.sync="totalRows"
              :isBusy="false"
              :selected.sync="selected"
              @edit="onClickEdit"
              @delete="row => { $confirmDelete(row); onClickDelete(row) }"
            ></app-table>

            {{selected}}
          </b-container>
        </div>
      `
    }),
    {
      info: '# Documentation'
    }
  )
