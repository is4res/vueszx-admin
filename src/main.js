// Polyfills
import 'es6-promise/auto'
import 'babel-polyfill'

import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'
import '@/registerServiceWorker'
import '@/i18n'

import BootstrapVue from 'bootstrap-vue'
import YmapPlugin from 'vue-yandex-maps'
import ComponentPlugin from '@/components/_ComponentPlugin'
import VeeValidate, { Validator } from 'vee-validate'
import en from 'vee-validate/dist/locale/en.js' // vee-validate locale
import th from 'vee-validate/dist/locale/th.js' // vee-validate locale
import localeEn from '@/i18n/en.json' // i18n locale
import localeTh from '@/i18n/th.json' // i18n locale
import VueSweetalert2 from 'vue-sweetalert2'
import VueApexCharts from 'vue-apexcharts'
import HighchartsVue from 'highcharts-vue'
import Highcharts from 'highcharts'
import exportingInit from 'highcharts/modules/exporting'
import stockInit from 'highcharts/modules/stock'
import Fragment from 'vue-fragment'
import '@/fontawesome'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'
import VCalendar from 'v-calendar'
import VuePerfectScrollbar from 'vue-perfect-scrollbar'

import lodash from 'lodash'
import request from '@/util/request'
import { token, permission } from '@/router/middleware'

/*
----------------
---- plugin ----
----------------
*/
Vue.use(BootstrapVue)
Vue.use(YmapPlugin)
Vue.use(ComponentPlugin)
Vue.use(VueSweetalert2)
Vue.use(VeeValidate, {
  locale: 'en',
  dictionary: {
    en: {
      ...en,
      attributes: localeEn.forms.label
    },
    th: {
      ...th,
      attributes: localeTh.forms.label
    }
  },
  // This is the default
  inject: true,
  // Important to name this something other than 'fields'
  fieldsBagName: 'veeFields',
  // This is not required but avoids possible naming conflicts
  errorBagName: 'veeErrors'
})
Vue.use(VCalendar)
Vue.use(HighchartsVue)
stockInit(Highcharts)
exportingInit(Highcharts)
Vue.use(Fragment.Plugin)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)
Vue.component('apexchart', VueApexCharts)
Vue.component('vue-perfect-scrollbar', VuePerfectScrollbar)

/*
----------------
---- global ----
----------------
*/
Vue.mixin({
  data () {
    return {
      lodash: lodash
    }
  },
  computed: {
    staticAlert () { // default alert modal pack
      return {
        success: {
          type: 'success',
          title: this.$t('alert.success.title'),
          text: this.$t('alert.success.text')
        },
        fail: {
          type: 'error',
          title: this.$t('alert.fail.title'),
          text: this.$t('alert.fail.text')
        },
        custom: {
          type: this.$store.getters.alert.type,
          title: this.$store.getters.alert.title,
          text: this.$store.getters.alert.text,
        }
      }
    }
  },
  mounted () {
    this.watchLocaleValidator()
    this.watchAlertModal()
  },
  methods: {
    $api: request, // request to api with axios
    watchLocaleValidator () { // watch locale to change validator locale
      this.$store.watch(
        (state, getters) => getters.config.locale,
        (locale) => {
          const compareLocale = {
            'gb': 'en',
            'th': 'th'
          }

          if (Validator.locale !== compareLocale[locale]) {
            Validator.localize(compareLocale[locale])
          }
        }
      )
    },
    watchAlertModal () { // watch to show alert modal
      this.$store.watch(
        (state, getters) => getters.alert.show,
        (show) => {
          if (show) {
            let status = this.$store.getters.alert.status
            if (this.staticAlert[status]) {
              this.$swal({
                type: this.staticAlert[status].type,
                title: this.staticAlert[status].title,
                text: this.staticAlert[status].text,
              })
            }

            // clear alert to default
            this.$store.commit('setAlert', {
              show: false,
              status: '',
              type: '',
              title: '',
              text: ''
            })
          }
        },
      )
    },
    $validateState (ref) { // validate on change input field
      if (
        this.veeFields[ref] &&
        (this.veeFields[ref].dirty || this.veeFields[ref].validated)
      ) {
        return !this.veeErrors.has(ref)
      }
      return null
    },
    $validateStateFeedback (ref) { // get error feedback from validate
      if (
        this.veeFields[ref] &&
        (this.veeFields[ref].dirty || this.veeFields[ref].validated)
      ) {
        let feedback = null
        if (this.veeErrors.has(ref)) {
          feedback = this.veeErrors.first(ref)
          if (ref.includes('-')) {
            let attr = ref.split('-')[0]
            feedback = feedback.replace(new RegExp(ref, 'g'), this.$t(`forms.label.${attr}`))
          }
        }
        return feedback
      }
      return null
    },
    $submitDelete (data) { // submit delete should be override in component
      return new Promise((resolve, reject) => {
        resolve(true)
      })
    },
    $confirmDelete (data) { // confirm delete modal
      this.$swal.fire({
        type: 'warning',
        title: this.$t('alert.submitDelete.title'),
        text: this.$t('alert.submitDelete.text'),
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: this.$t('alert.submitDelete.confirmButtonText')
      }).then((result) => {
        if (result.value) {
          if (data.length === 0) {
            // minimum select atleast one
            this.$store.commit('setAlert', {
              show: true,
              status: 'custom',
              type: 'warning',
              title: this.$t('alert.submitDelete.warning.title'),
              text: this.$t('alert.submitDelete.warning.text')
            })
            return
          }

          // call submitDelete to do what it should
          this.$submitDelete(data).then(() => {
            this.$store.commit('setAlert', {
              show: true,
              status: 'custom',
              type: 'success',
              title: this.$t('alert.submitDelete.done.title'),
              text: this.$t('alert.submitDelete.done.text')
            })
          })
        }
      })
    },
  },
})

/*
----------------
-- middleware --
----------------
*/
router.beforeEach(async (to, from, next) => {
  let changeNext

  store.commit('setLoading', true) // show loading page

  /**
   * auth middleware
   */
  if (to.meta.auth) {
    // validate token
    if (!token()) {
      changeNext = 'login'
    }

    // validate permission for access page
    if (!permission(to.meta.auth, store.getters.userPermission)) {
      changeNext = 'exception404'
    }
  }

  /**
   * some route can't be access after login
   */
  if (to.meta.authCantAccess) {
    if (from.name !== 'logout' && to.name !== 'login') { // login can't access but if come from logout page it can
      if (token()) {
        changeNext = 'exception404'
      }
    }
  }

  if (changeNext) {
    if (from.name === changeNext) { // hide loading page if from and next is same route
      store.commit('setLoading', false)
    }

    changeNext = { name: changeNext }
  }

  return next(changeNext)
})

router.afterEach((to, from) => {
  store.commit('setLoading', false) // hide loading page
})

// Start
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
