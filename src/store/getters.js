const config = state => state.app.config
const palette = state => state.app.config.palette
const locale = state => state.app.config.locale
const isLoading = state => state.app.isLoading
const classToggleSidebar = state => state.app.config.classToggleSidebar
const userPermission = state => state.app.userPermission
const alert = state => state.app.alert

export {
  config,
  palette,
  locale,
  isLoading,
  classToggleSidebar,
  userPermission,
  alert
}
